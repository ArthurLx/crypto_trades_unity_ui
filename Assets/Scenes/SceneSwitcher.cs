using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneSwitcher : MonoBehaviour
{
    public void SubscribeOnTrades()
    {
        GameObject goBtnSelected = EventSystem.current.currentSelectedGameObject;

        GameObject goPanelContainingBtn = goBtnSelected.transform.parent.gameObject;

        PairsData.PairInputStr = goPanelContainingBtn.transform.Find("PairName").GetComponent<Text>().text;

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
