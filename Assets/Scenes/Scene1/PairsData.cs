using Newtonsoft.Json.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PairsData : MonoBehaviour
{
    public GameObject PairEntryPrefab;
    public GameObject SpacePanelPrefab;

    public static string PairInputStr { get; set; }

    // Start is called before the first frame update
    public async void Start()
    {
        var url = "https://api.binance.com/api/v3/exchangeInfo";

        UnityWebRequest www = UnityWebRequest.Get(url);
        www.SetRequestHeader("Content-Type", "application/json");

        var operation = www.SendWebRequest();

        while (!operation.isDone)
        {
            await Task.Yield();
        }
        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.Log("Error at WebRequest Pairs!!");
            return;
        }

        var jsonResponse = www.downloadHandler.text;

        var token = JToken.Parse(jsonResponse);
        var symbols = token.Value<JArray>("symbols");
        var count = symbols.Count;

        var tradingPairList = new List<TradingPair>();

        for (int i = 0; i < count; i++)
        {
            var tradingPair = new TradingPair();
            tradingPair.PairName = symbols[i].SelectToken($"symbol").Value<string>();
            tradingPair.Status = symbols[i].SelectToken($"status").Value<string>();
            tradingPairList.Add(tradingPair);
        }

        GameObject goSP = Instantiate(SpacePanelPrefab);
        goSP.transform.SetParent(this.transform);
        goSP.transform.Find("InfoText").GetComponent<Text>().text = "Most Popular:";

        for (int i = 0; i < count; i++)
        {
            if (tradingPairList[i].Status == "TRADING")
            {
                switch (tradingPairList[i].PairName)
                {
                    case "BTCUSDT":
                    case "ETHUSDT":
                    case "ETHBTC":
                    case "XRPUSDT":
                    case "BTCBUSD":
                        GameObject go = Instantiate(PairEntryPrefab);
                        go.transform.SetParent(this.transform);

                        go.transform.Find("PairName").GetComponent<Text>().text = tradingPairList[i].PairName;
                        go.transform.Find("PairStatus").GetComponent<Text>().text = tradingPairList[i].Status;
                        break;
                    default:
                        break;
                }
            }
        }

        GameObject goSP2 = Instantiate(SpacePanelPrefab);
        goSP2.transform.SetParent(this.transform);
        goSP2.transform.Find("InfoText").GetComponent<Text>().text = "All Trading Pairs:";

        for (int i = 0; i < count; i++)
        {
            GameObject go = Instantiate(PairEntryPrefab);
            go.transform.SetParent(this.transform);

            go.transform.Find("PairName").GetComponent<Text>().text = tradingPairList[i].PairName;
            go.transform.Find("PairStatus").GetComponent<Text>().text = tradingPairList[i].Status;
        }

    }

    // Update is called once per frame
    void Update()
    {

    }
}
