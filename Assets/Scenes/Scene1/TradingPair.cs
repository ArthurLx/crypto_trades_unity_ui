using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TradingPair
{
    public string PairName { get; set; }
    public string Status { get; set; }
}
