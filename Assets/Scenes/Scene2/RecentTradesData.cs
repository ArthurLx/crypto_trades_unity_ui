using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class RecentTradesData : MonoBehaviour
{
    public GameObject TradeEntryPrefab;

    // Start is called before the first frame update
    public async void Start()
    {
        var limit = "&limit=2";
        var url = "https://api.binance.com/api/v3/trades?symbol=" + PairsData.PairInputStr + limit;
        while (true)
        {
            UnityWebRequest www = UnityWebRequest.Get(url);
            www.SetRequestHeader("Content-Type", "application/json");

            var operation = www.SendWebRequest();

            while (!operation.isDone)
            {
                await Task.Yield();
            }
            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log("Error at WebRequest Traids!!");
                return;
            }

            var jsonResponse = www.downloadHandler.text;

            var parsed = JArray.Parse(jsonResponse);
            var count = parsed.Count;

            var recentTradeList = new List<RecentTrade>();

            for (int i = 0; i < count; i++)
            {
                var recentTrade = new RecentTrade();

                recentTrade.pair = PairsData.PairInputStr;
                recentTrade.price = parsed[i].SelectToken("price").Value<double>();
                recentTrade.amount = parsed[i].SelectToken("qty").Value<double>();
                recentTrade.time = parsed[i].SelectToken("time").Value<long>();
                recentTrade.isBuyerMaker = parsed[i].SelectToken("isBuyerMaker").Value<bool>();

                recentTradeList.Add(recentTrade);
            }

            DateTimeOffset dateTimeOffset;

            for (int i = 0; i < count; i++)
            {
                string pair_str = recentTradeList[i].pair;
                string price_str = String.Format("{0:F2}", recentTradeList[i].price);
                string amount_str = String.Format("{0:F5}", recentTradeList[i].amount);

                dateTimeOffset = DateTimeOffset.FromUnixTimeMilliseconds(recentTradeList[i].time);
                string time_str = dateTimeOffset.LocalDateTime.ToLongTimeString();

                GameObject go = Instantiate(TradeEntryPrefab);
                go.transform.SetParent(this.transform);

                go.transform.Find("TradingPair").GetComponent<Text>().text = pair_str;
                go.transform.Find("TradingPrice").GetComponent<Text>().text = price_str;

                if (recentTradeList[i].isBuyerMaker == true)
                {
                    go.transform.Find("TradingPrice").GetComponent<Text>().color = Color.red;
                }
                else
                {
                    go.transform.Find("TradingPrice").GetComponent<Text>().color = Color.green;
                }

                go.transform.Find("TradingAmount").GetComponent<Text>().text = amount_str;
                go.transform.Find("TradingTime").GetComponent<Text>().text = time_str;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
